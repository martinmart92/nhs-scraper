package fr.martin.babylon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by martin on 05/08/16.
 */
public class Main {

    static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        LOG.info("starting nhs-scraper app");
        ScrapingEngine engine = new ScrapingEngine();

        engine.init();

        engine.start();

        engine.stop();

        engine.report();
        LOG.info("finish app");
    }
}
