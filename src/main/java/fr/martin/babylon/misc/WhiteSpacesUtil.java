package fr.martin.babylon.misc;

import javax.annotation.Nonnull;

/**
 * Created by martin on 14/08/16.
 */
public class WhiteSpacesUtil {

    public static final char NON_BREAKING_SPACE_CHAR = (char) 160;
    public static final String REAL_SPACE_CHAR = " ";

    /**
     * replace strange spaces chars by real space char
     * @param s input string containing maybe some strange space char
     * @return
     */
    @Nonnull
    public static String clean(@Nonnull String s) {
        return s.replace(String.valueOf(NON_BREAKING_SPACE_CHAR), REAL_SPACE_CHAR).trim();
    }
}
