package fr.martin.babylon;

import fr.martin.babylon.download.JSoupDownloaderImpl;
import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.logics.impl.HubScrapingLogicImpl;
import fr.martin.babylon.model.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by martin on 05/08/16.
 */
public class ScrapingDriver {

    public static final int N_THREADS = 10;
    private static final Logger LOG = LoggerFactory.getLogger(ScrapingDriver.class);

    private List<ScrapingListenerIfc> listeners;
    private ExecutorService executor;

    public ScrapingDriver(@Nonnull List<ScrapingListenerIfc> listeners) {
        this.listeners = listeners;
    }

    public void process() {

        onStarted();

        initExecutor();

        ScrapingLogicIfc initial = new HubScrapingLogicImpl();
        initial.init(Constants.NHS_ROOT_HUB, new JSoupDownloaderImpl());

        executeLogics(Collections.singletonList(initial));
    }


    private void executeLogics(@Nonnull List<ScrapingLogicIfc> logics) {
        List<ScrapingLogicIfc> filteredLogics = logics.stream()
                .filter(l -> !isStopUrl(l.getUrl()))
                .filter(l -> !alreadyVisited(l.getUrl()))
                .collect(Collectors.toList());


        List<Callable<List<ScrapingLogicIfc>>> callables = filteredLogics.stream()
                .map(l -> {

                    return (Callable<List<ScrapingLogicIfc>>) () -> {
                        try {
                            Page page = l.scrap();

                            onPageCreated(page);

                            addToVisited(l.getUrl());


                            return l.nextLogics();
                        } catch (ScrapingException e) {
                            onScrapingError(l.getUrl(), e);
                        }
                        return Collections.emptyList();
                    };
                })
                .collect(Collectors.toList());

        try {
            executor.invokeAll(callables)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        }
                        catch (Exception e) {
                            throw new IllegalStateException(e);
                        }
                    })
                    .forEach(this::executeLogics);
        } catch (InterruptedException e) {

            e.printStackTrace();
            System.err.print("interruption exception occurred");
        }
    }


    private void addToVisited(@Nonnull String url) {
        World.INSTANCE.getVisitedUrls().add(url);
    }

    private boolean alreadyVisited(@Nonnull String url) {
        return World.INSTANCE.getVisitedUrls().contains(url);
    }


    private boolean isStopUrl(String url) {
        return World.INSTANCE.getStopUrls().contains(url);

    }

    private void onScrapingError(@Nonnull String url, @Nonnull ScrapingException e) {

        for(ScrapingListenerIfc listener : listeners) {
            listener.onError(url, e);
        }
    }

    public void onPageCreated(@Nonnull Page page) {

        for(ScrapingListenerIfc listener : listeners) {
            listener.onCreatePage(page);
        }

    }

    public void onStarted() {
        for(ScrapingListenerIfc listener : listeners) {
            listener.onStart();
        }
    }

    public void onFinished() {

        closeExecutor();

        for(ScrapingListenerIfc listener : listeners) {
            listener.onFinish();
        }
    }

    private void initExecutor() {
        executor = Executors.newFixedThreadPool(N_THREADS);
    }
    private void closeExecutor() {

        try {
            LOG.info("try close executor");
            executor.shutdown();
            executor.awaitTermination(10, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            LOG.error("InterruptedException", e);
        }
        finally {
            if (!executor.isTerminated()) {
                LOG.warn("executor has tasks not yet terminated, forcing close");
            }
            LOG.info("now close executor");
            executor.shutdownNow();
            LOG.info("close executor finished");
        }
    }

    @Nonnull
    public String report() {
        StringBuilder sb = new StringBuilder();
        for(ScrapingListenerIfc listener : listeners) {
            sb.append(listener.askReport());
            sb.append("\n");
        }
        return sb.toString();
    }
}
