package fr.martin.babylon;

/**
 * Created by martin on 05/08/16.
 */
public class Constants {

    //http://www.nhs.uk/Conditions/Pages/hub.aspx
    public static final String NHS_ROOT = "http://www.nhs.uk";
    public static final String NHS_ROOT_PAGE = NHS_ROOT + "/Conditions/Pages";
    public static final String NHS_ROOT_HUB = NHS_ROOT_PAGE + "/hub.aspx";
    public static final String HTTP = "http";

    public static final String OUTPUT_FOLDER = "/home/martin/IdeaProjects/nhs-scraper/tmp/output";

}
