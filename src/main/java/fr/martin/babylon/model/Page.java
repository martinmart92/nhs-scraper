package fr.martin.babylon.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * POJO defining a web page
 * Created by martin on 04/08/16.
 */
public class Page {

    private String title;
    private String url;
    private String html;
    private String uuid;
    private String description;
    private String keywords;

    @JsonIgnore
    private boolean placeholder = false;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public boolean isPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(boolean placeholder) {
        this.placeholder = placeholder;
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Page page = (Page) o;

        if (placeholder != page.placeholder) return false;
        if (title != null ? !title.equals(page.title) : page.title != null) return false;
        if (url != null ? !url.equals(page.url) : page.url != null) return false;
        if (html != null ? !html.equals(page.html) : page.html != null) return false;
        if (uuid != null ? !uuid.equals(page.uuid) : page.uuid != null) return false;
        if (description != null ? !description.equals(page.description) : page.description != null) return false;
        return keywords != null ? keywords.equals(page.keywords) : page.keywords == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (html != null ? html.hashCode() : 0);
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (keywords != null ? keywords.hashCode() : 0);
        result = 31 * result + (placeholder ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Page{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", html='" + html + '\'' +
                ", uuid='" + uuid + '\'' +
                ", description='" + description + '\'' +
                ", keywords='" + keywords + '\'' +
                ", placeholder=" + placeholder +
                '}';
    }
}
