package fr.martin.babylon.model;

/**
 * Created by martin on 06/08/16.
 */
public class ConceptPage extends Page {

    private String conceptName;


    public String getConceptName() {
        return conceptName;
    }

    public void setConceptName(String conceptName) {
        this.conceptName = conceptName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ConceptPage that = (ConceptPage) o;

        return conceptName != null ? conceptName.equals(that.conceptName) : that.conceptName == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (conceptName != null ? conceptName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ConceptPage{" +
                "conceptName='" + conceptName + '\'' +
                "} " + super.toString();
    }
}
