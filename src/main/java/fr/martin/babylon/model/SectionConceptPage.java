package fr.martin.babylon.model;

/**
 * Created by martin on 06/08/16.
 */
public class SectionConceptPage extends ConceptPage {

    private String sectionType;
    private String BMSection1;
    private String BMSection2;
    private String BMSection3;

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }


    public String getBMSection1() {
        return BMSection1;
    }

    public void setBMSection1(String BMSection1) {
        this.BMSection1 = BMSection1;
    }

    public String getBMSection2() {
        return BMSection2;
    }

    public void setBMSection2(String BMSection2) {
        this.BMSection2 = BMSection2;
    }

    public String getBMSection3() {
        return BMSection3;
    }

    public void setBMSection3(String BMSection3) {
        this.BMSection3 = BMSection3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SectionConceptPage that = (SectionConceptPage) o;

        if (sectionType != null ? !sectionType.equals(that.sectionType) : that.sectionType != null) return false;
        if (BMSection1 != null ? !BMSection1.equals(that.BMSection1) : that.BMSection1 != null) return false;
        if (BMSection2 != null ? !BMSection2.equals(that.BMSection2) : that.BMSection2 != null) return false;
        return BMSection3 != null ? BMSection3.equals(that.BMSection3) : that.BMSection3 == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (sectionType != null ? sectionType.hashCode() : 0);
        result = 31 * result + (BMSection1 != null ? BMSection1.hashCode() : 0);
        result = 31 * result + (BMSection2 != null ? BMSection2.hashCode() : 0);
        result = 31 * result + (BMSection3 != null ? BMSection3.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SectionConceptPage{" +
                "sectionType='" + sectionType + '\'' +
                ", BMSection1='" + BMSection1 + '\'' +
                ", BMSection2='" + BMSection2 + '\'' +
                ", BMSection3='" + BMSection3 + '\'' +
                "} " + super.toString();
    }
}
