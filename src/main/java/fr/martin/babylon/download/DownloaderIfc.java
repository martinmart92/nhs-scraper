package fr.martin.babylon.download;

import org.jsoup.nodes.Document;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Created by martin on 06/08/16.
 */
public interface DownloaderIfc {

    @Nonnull
    Document get(@Nonnull String url) throws IOException;

    @Nonnull String getUpdatedUrl();
}
