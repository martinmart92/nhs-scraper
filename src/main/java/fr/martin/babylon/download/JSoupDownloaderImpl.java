package fr.martin.babylon.download;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Created by martin on 06/08/16.
 */
public class JSoupDownloaderImpl implements DownloaderIfc {
    public static final int TIMEOUT = 10000;

    private String url;
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Nonnull
    @Override
    public Document get(@Nonnull String url) throws IOException {
        this.url = url;
        LOG.trace("downloading page at url : "+url);
        Connection.Response response = Jsoup.connect(url)
                .followRedirects(true)
                .timeout(TIMEOUT)
                .execute();
        //update in case of forwarding
        this.url = response.url().toExternalForm();
        return response.parse();
    }

    @Nonnull
    @Override
    public String getUpdatedUrl() {
        return this.url;
    }
}
