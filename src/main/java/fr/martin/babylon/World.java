package fr.martin.babylon;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by martin on 05/08/16.
 */
public class World {

    public static final World INSTANCE = new World();
    private Set<String> visitedUrls = new HashSet<>();
    private Set<String> stopUrls = new HashSet<>();

    private  World() {
        initStopUrls();
    }

    private void initStopUrls() {
        getStopUrls().add("http://www.nhs.uk/Conditions/Pages/#headertop");//back to top link
        getStopUrls().add("http://www.nhs.uk/cardiomyopathy");//dead link : 404
    }

    public Set<String> getVisitedUrls() {
        return visitedUrls;
    }


    public Set<String> getStopUrls() {
        return stopUrls;
    }
}
