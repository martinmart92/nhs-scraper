package fr.martin.babylon;

import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.listeners.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 05/08/16.
 */
public class ScrapingEngine {

    private ScrapingDriver driver;
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    public ScrapingEngine(){

    }

    public void init() {
        driver = new ScrapingDriver(initListeners());
    }
    public void start() {
        driver.process();
    }

    public void stop() {
        driver.onFinished();
    }

    public void report() {
        LOG.info("REPORT : \n"+driver.report());
    }


    private List<ScrapingListenerIfc> initListeners() {
        ScrapingListenerIfc counter = new CountListenerImpl();
        ScrapingListenerIfc jsonSerializer = new JSONSerializerListenerImpl();
        ScrapingListenerIfc errors = new ErrorListenerImpl();
        ScrapingListenerIfc urls = new PagesUrlListenerImpl();
        ScrapingListenerIfc times = new TimeListenerImpl();
        List<ScrapingListenerIfc> list = new ArrayList<>();
        list.add(counter);
        list.add(jsonSerializer);
        list.add(errors);
        list.add(urls);
        list.add(times);
        return list;
    }

}
