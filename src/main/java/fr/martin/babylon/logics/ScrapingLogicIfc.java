package fr.martin.babylon.logics;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.download.DownloaderIfc;
import fr.martin.babylon.model.Page;
import java.util.List;
import javax.annotation.Nonnull;

/**
 * Created by martin on 05/08/16.
 */
public interface ScrapingLogicIfc {

    void init(@Nonnull String url, @Nonnull DownloaderIfc downloader);

    @Nonnull
    String getUrl();

    @Nonnull
    Page scrap() throws ScrapingException;

    @Nonnull
    List<ScrapingLogicIfc> nextLogics();

}
