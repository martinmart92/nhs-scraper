package fr.martin.babylon.logics.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.download.JSoupDownloaderImpl;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.misc.WhiteSpacesUtil;
import fr.martin.babylon.model.ConceptPage;
import fr.martin.babylon.model.Page;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * scraping logic dedicated to download "concept" pages, such as: http://www.nhs.uk/conditions/Altitude-sickness
 * Created by martin on 05/08/16.
 */
public class ConceptPageScrapingLogicImpl extends AbstractScrapingLogic implements ScrapingLogicIfc {
    @Nonnull
    @Override
    public Page scrap() throws ScrapingException {
        try {

            Document doc = downloadDocument();

            ConceptPage page = createConceptPage(doc);

            Elements cleanTitles = doc.select(".healthaz-header h1");
            Optional<String> headerOpt = cleanTitles.stream()
                    .map(title -> WhiteSpacesUtil.clean(title.text()).trim())
                    .findFirst();

            headerOpt.ifPresent(h -> {
                ConceptNameUtil util = new ConceptNameUtil();
                page.setConceptName(util.processHeader(h));
            });

            Elements sections = doc.select("#ctl00_PlaceHolderMain_articles a");
            List<ScrapingLogicIfc> logics = sections.stream()
                    .map(sectionA -> sectionA.attr("href").trim())
                    .map(link -> pseudoToFull(link))
                    .map(link -> {
                        ScrapingLogicIfc logic = new ConceptSectionPageScrapingLogicImpl();
                        logic.init(link, new JSoupDownloaderImpl());
                        return logic;
                    })
                    .collect(Collectors.toList());

            nextLogics.addAll(logics);



            return page;
        } catch (IOException e) {
            throw new ScrapingException("unable to scrap hub page", e);
        }
    }

}
