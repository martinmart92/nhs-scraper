package fr.martin.babylon.logics.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.misc.WhiteSpacesUtil;
import fr.martin.babylon.model.Page;
import fr.martin.babylon.model.SectionConceptPage;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by martin on 06/08/16.
 */
public class ConceptSectionPageScrapingLogicImpl extends AbstractScrapingLogic implements ScrapingLogicIfc {

    @Nonnull
    @Override
    public Page scrap() throws ScrapingException {
        try {

            Document doc = downloadDocument();

            SectionConceptPage page = createSectionConceptPage(doc);

            Elements cleanTitles = doc.select(".healthaz-content h2");
            Optional<String> headerOpt = cleanTitles.stream()
                    .map(title -> cleanStrangeWhiteSpaces(title.text()).trim())
                    .findFirst();

            headerOpt.ifPresent(h-> {
                ConceptNameUtil util = new ConceptNameUtil();
                page.setConceptName(util.processHeader(h));
            });

            Arrays.stream(SectionTypeEnum.values())
                    .forEach(sectionType -> {
                        if (url.toLowerCase().contains(sectionType.getValue())) {
                            page.setSectionType(sectionType.getValue());
                        }
                        else {
                            LOG.trace("section type not found : "+url);
                        }
                    });

            //metas
            extractBMSection1(doc, page);
            extractBMSection2(doc, page);
            extractBMSection3(doc, page);

            return page;
        } catch (IOException e) {
            throw new ScrapingException("unable to scrap hub page", e);
        }
    }

    private String cleanStrangeWhiteSpaces(String text) {
        return WhiteSpacesUtil.clean(text);
    }

    private void extractBMSection1(Document doc, SectionConceptPage page) {
        extractMeta(doc, "DCSext.BM_Section1")
                .ifPresent(v -> page.setBMSection1(cleanStrangeWhiteSpaces(v)));
    }

    private void extractBMSection2(Document doc, SectionConceptPage page) {
        extractMeta(doc, "DCSext.BM_Section2")
                .ifPresent(v -> page.setBMSection2(cleanStrangeWhiteSpaces(v)));
    }
    private void extractBMSection3(Document doc, SectionConceptPage page) {
        extractMeta(doc, "DCSext.BM_Section3")
                .ifPresent(v -> page.setBMSection3(cleanStrangeWhiteSpaces(v)));
    }

}
