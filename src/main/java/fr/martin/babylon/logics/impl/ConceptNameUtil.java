package fr.martin.babylon.logics.impl;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by martin on 13/08/16.
 */
public class ConceptNameUtil {



    /**
     * heuristic to extract interesting concepts from String
     * @param header
     * @return
     */
    @Nonnull
    protected String processHeader(@Nonnull String header) {


        List<String> stops = new ArrayList<String>();
        stops.add("About your ");
        stops.add("Alternatives to ");
        stops.add("Risks of an ");
        stops.add("Preventing ");
        stops.add("Avoiding ");
        stops.add("Results of ");
        stops.add("Side effects of ");
        stops.add("Signs and symptoms of ");
        stops.add("Symptoms of a ");
        stops.add("Symptoms of ");
        stops.add("Causes of a ");
        stops.add("Causes of ");
        stops.add("Diagnosing a ");
        stops.add("Diagnosing ");
        stops.add("Treating a ");
        stops.add("Treating ");
        stops.add("Treatment and support for people with ");
        stops.add("Treatment of a ");
        stops.add("Treatment of ");
        stops.add("Complications of a ");
        stops.add("Complications of ");
        stops.add("Features of a ");
        stops.add("Features of ");
        stops.add("Living with a ");
        stops.add("Living with ");
        stops.add("Recovering from a ");
        stops.add("Recovering from ");
        stops.add("How a ");
        stops.add("How ");
        stops.add("Why a ");
        stops.add("Why ");
        for (String stop : stops) {
            if (header.contains(stop)) {
                return header.replace(stop, "").trim();
            }
        }
        //in case not found
        return header.trim();
    }
}
