package fr.martin.babylon.logics.impl;

import fr.martin.babylon.Constants;
import fr.martin.babylon.Main;
import fr.martin.babylon.download.DownloaderIfc;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.model.ConceptPage;
import fr.martin.babylon.model.Page;
import fr.martin.babylon.model.SectionConceptPage;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by martin on 05/08/16.
 */
public abstract class AbstractScrapingLogic implements ScrapingLogicIfc {

    protected String url;
    protected List<ScrapingLogicIfc> nextLogics = new ArrayList<>();
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());
    private DownloaderIfc downloader;

    @Override
    public void init(@Nonnull String url, @Nonnull DownloaderIfc downloader) {
        this.url = url;
        this.downloader = downloader;

    }

    @Nonnull
    @Override
    public String getUrl() {
        return this.url;
    }

    @Nonnull
    protected Document downloadDocument() throws IOException {
        long start = System.currentTimeMillis();
        LOG.info("downloading start-url : "+this.url);
        Document doc = downloader.get(this.url);
        this.url = downloader.getUpdatedUrl();
        LOG.info("downloaded actual-url : "+this.url+
                " in TIME = "+(System.currentTimeMillis()-start)+"ms.");
        return doc;
    }

    protected Page createPage(Document doc) {

        Page page = new Page();
        return fillPageFromDoc(page, doc);

    }
    protected ConceptPage createConceptPage(Document doc) {

        ConceptPage page = new ConceptPage();
        return (ConceptPage)fillPageFromDoc(page, doc);
    }

    protected SectionConceptPage createSectionConceptPage(Document doc){
        SectionConceptPage page = new SectionConceptPage();
        return (SectionConceptPage)fillPageFromDoc(page, doc);
    }

    private Page fillPageFromDoc(@Nonnull Page page, @Nonnull Document doc) {
        page.setUuid(UUID.randomUUID().toString());
        page.setUrl(url);
        page.setTitle(doc.title());
        page.setHtml(doc.html());
        extractMeta(doc, "DC.description").ifPresent(d->page.setDescription(d));
        extractMeta(doc, "keywords").ifPresent(k->page.setKeywords(k));
        return page;
    }

    protected static String pseudoToFull(String pseudoLink) {
        pseudoLink = pseudoLink.trim();
        if (pseudoLink.startsWith(Constants.HTTP)) {
            return pseudoLink;
        }
        if (pseudoLink.startsWith("/")) {
            return Constants.NHS_ROOT+pseudoLink;
        }

        return Constants.NHS_ROOT_PAGE+"/"+pseudoLink;

    }

    protected Optional<String> extractMeta(Document doc, String name) {
        Elements elements = doc.select("head meta[name='"+name+"']");
        return elements.stream()
                .map(metaEl -> metaEl.attr("content").trim())
                .findFirst();
    }

    @Nonnull
    @Override
    public List<ScrapingLogicIfc> nextLogics() {
        return nextLogics;
    }

}
