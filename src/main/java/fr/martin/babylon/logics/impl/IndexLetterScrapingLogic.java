package fr.martin.babylon.logics.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.download.JSoupDownloaderImpl;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.model.Page;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 *
 * scraping logic to download "letter" pages such as : http://www.nhs.uk/conditions/pages/BodyMap.aspx?Index=A
 * Created by martin on 05/08/16.
 */
public class IndexLetterScrapingLogic extends AbstractScrapingLogic {

    @Nonnull
    @Override
    public Page scrap() throws ScrapingException {
        try {

            Document doc = downloadDocument();

            Page page = createPage(doc);
            page.setPlaceholder(true);

            Elements pageLinksElts = doc.select("#ctl00_PlaceHolderMain_BodyMap_ConditionsByAlphabet a");

            for (Element pageLinkElt : pageLinksElts) {
                String pageLinkURL = pageLinkElt.attr("href");

                String pageFullLink = pseudoToFull(pageLinkURL);

                ScrapingLogicIfc logic = new ConceptPageScrapingLogicImpl();
                logic.init(pageFullLink, new JSoupDownloaderImpl());

                nextLogics.add(logic);
            }
            return page;
        } catch (IOException e) {
            throw new ScrapingException("unable to scrap hub page", e);
        }
    }
}
