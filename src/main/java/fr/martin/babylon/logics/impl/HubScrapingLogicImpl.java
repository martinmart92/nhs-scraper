package fr.martin.babylon.logics.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.download.JSoupDownloaderImpl;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.model.Page;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 *
 * scraping logic to download page : http://www.nhs.uk/Conditions/Pages/hub.aspx
 *
 * Created by martin on 05/08/16.
 */
public class HubScrapingLogicImpl extends AbstractScrapingLogic {

    @Nonnull
    @Override
    public Page scrap() throws ScrapingException {

        try {
            Document doc = downloadDocument();

            Page page = createPage(doc);
            page.setPlaceholder(true);
            Elements alphabet = doc.select("#haz-mod1 ul li a");
            for (Element letter : alphabet) {
                String pseudoLink = letter.attr("href");
                String fullLink = pseudoToFull(pseudoLink);

                ScrapingLogicIfc logic = new IndexLetterScrapingLogic();
                logic.init(fullLink, new JSoupDownloaderImpl());

                nextLogics.add(logic);
            }
            return page;
        } catch (IOException e) {
            throw new ScrapingException("unable to scrap hub page", e);
        }
    }

}
