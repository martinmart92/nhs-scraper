package fr.martin.babylon.logics.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 14/08/16.
 */
public enum SectionTypeEnum {

    SYMPTOMS("symptoms"),
    TREATMENT("treatment"),
    COMPLICATIONS("complications"),
    PREVENTION("prevention"),
    DIAGNOSIS("diagnosis"),
    CAUSES("causes"),
    RISKS("risks")
    ;


    public String getValue() {
        return value;
    }

    private String value;

    SectionTypeEnum(String value) {
        this.value = value;
    }
}
