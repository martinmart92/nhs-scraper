package fr.martin.babylon.listeners;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.model.Page;

import javax.annotation.Nonnull;

/**
 * Created by martin on 05/08/16.
 */
public interface ScrapingListenerIfc {

    void onStart();

    void onCreatePage(@Nonnull Page page);

    void onFinish();

    void onError(@Nonnull String url, @Nonnull ScrapingException e);

    @Nonnull
    String askReport();
}
