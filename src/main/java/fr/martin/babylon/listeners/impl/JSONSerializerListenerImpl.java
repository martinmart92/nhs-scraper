package fr.martin.babylon.listeners.impl;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.martin.babylon.Constants;
import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.model.Page;
import org.apache.commons.io.FileUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by martin on 05/08/16.
 */
public class JSONSerializerListenerImpl implements ScrapingListenerIfc {

    private ObjectMapper mapper = new ObjectMapper();

    private StringBuilder sb = new StringBuilder();

    private File outputParentFolder;

    @Override
    public void onStart() {

        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);

        outputParentFolder = new File(Constants.OUTPUT_FOLDER);
        if (!outputParentFolder.exists()) {
            outputParentFolder.mkdirs();
        }
        else {
            try {
                FileUtils.cleanDirectory(outputParentFolder);
            } catch (IOException ignored) {
                ignored.printStackTrace();
            }
        }
    }

    @Override
    public void onCreatePage(@Nonnull Page page) {

        if (page.isPlaceholder()) {
            //skip serialization
            return;
        }
        try {

            File file = prepareOutputFile(page);
            mapper.writeValue(file, page);
        } catch (IOException e) {
            e.printStackTrace();
            sb.append("error occurred while serializing page at "+page.getUrl());
            //sb.append(page.toString());
            sb.append("\n");
        }


    }

    @Nonnull
    private File prepareOutputFile(Page page) throws IOException {
        File f = new File(outputParentFolder, generateFileName(page));
        f.createNewFile();
        return f;
    }

    @Nonnull
    private String generateFileName(Page page) {
        if (page.getUuid()!=null) {
            return page.getUuid() + ".json";
        }
        return UUID.randomUUID().toString() + ".json";
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onError(@Nonnull String url, @Nonnull ScrapingException e) {

    }

    @Override
    @Nonnull
    public String askReport() {
        String errors = sb.toString();
        if (errors.isEmpty()) {
            errors = "no errors.";
        }
        return this.getClass().getSimpleName()
                + " reports: "+errors;
    }
}
