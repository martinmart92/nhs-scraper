package fr.martin.babylon.listeners.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.model.Page;

import javax.annotation.Nonnull;

/**
 * Created by martin on 05/08/16.
 */
public class CountListenerImpl implements ScrapingListenerIfc {

    private int pagesCreated = 0;
    private int errors = 0;

    @Override
    public void onStart() {

    }
    @Override
    public void onCreatePage(@Nonnull Page page) {
        pagesCreated++;
    }
    @Override
    public void onFinish() {

    }

    @Override
    public void onError(@Nonnull String url, @Nonnull ScrapingException e) {
        errors++;
    }

    @Override
    @Nonnull
    public String askReport() {

        return this.getClass().getSimpleName() + " reports "
                + pagesCreated + " pages created and "
                + errors + " errors.";
    }
}
