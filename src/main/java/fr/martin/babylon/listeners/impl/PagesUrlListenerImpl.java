package fr.martin.babylon.listeners.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.model.Page;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by martin on 05/08/16.
 */
public class PagesUrlListenerImpl implements ScrapingListenerIfc {

    private Map<String, String> titlesByUrls = new HashMap<>();

    @Override
    public void onStart() {

    }

    @Override
    public void onCreatePage(@Nonnull Page page) {
        titlesByUrls.put(page.getUrl(), page.getTitle());
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onError(@Nonnull String url, @Nonnull ScrapingException e) {

    }

    @Override
    @Nonnull
    public String askReport() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : titlesByUrls.entrySet()) {
            String url = entry.getKey();
            String title = entry.getValue();
            sb.append("scraped URL: "+url+", title: "+title);
            sb.append("\n");
        }

        return this.getClass().getSimpleName()
                + " reports following url scraping: \n"
                + sb.toString();
    }
}
