package fr.martin.babylon.listeners.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.model.Page;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by martin on 05/08/16.
 */
public class ErrorListenerImpl implements ScrapingListenerIfc{
    private Map<String, ScrapingException> exceptionsByUrl = new HashMap<>();
    @Override
    public void onStart() {

    }

    @Override
    public void onCreatePage(@Nonnull Page page) {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onError(@Nonnull String url, @Nonnull ScrapingException e) {
        exceptionsByUrl.put(url, e);
    }

    @Override
    @Nonnull
    public String askReport() {

        if (exceptionsByUrl.isEmpty()) {
            return this.getClass().getSimpleName()
                    + " reports no errors.";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, ScrapingException> entry : exceptionsByUrl.entrySet()) {
            //e.printStackTrace();
            sb.append("Error with url : '"+entry.getKey()+"': \n");
            sb.append(entry.getValue().getMessage()+", ");
            sb.append(entry.getValue().getCause().getMessage());
            sb.append("\n");
        }

        return this.getClass().getSimpleName()
                + " reports following errors : \n"
                + sb.toString();
    }
}
