package fr.martin.babylon.listeners.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.listeners.ScrapingListenerIfc;
import fr.martin.babylon.model.Page;

import javax.annotation.Nonnull;

/**
 * Created by martin on 05/08/16.
 */
public class TimeListenerImpl implements ScrapingListenerIfc {


    private long start;
    private long end;

    @Override
    public void onStart() {
        start = System.currentTimeMillis();
    }

    @Override
    public void onCreatePage(@Nonnull Page page) {

    }

    @Override
    public void onFinish() {
        end = System.currentTimeMillis();

    }

    @Override
    public void onError(@Nonnull String url, @Nonnull ScrapingException e) {

    }

    @Override
    @Nonnull
    public String askReport() {

        return this.getClass().getSimpleName()
                + " reports processing time: \n"
                + (end-start)
                + " ms.";
    }
}
