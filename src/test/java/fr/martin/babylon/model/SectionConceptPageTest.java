package fr.martin.babylon.model;

import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;

/**
 * testing {@link SectionConceptPage} class
 * Created by martin on 06/08/16.
 */
public class SectionConceptPageTest {


    @Test
    public void testBean() {
        assertThat(SectionConceptPage.class, allOf(
                hasValidBeanConstructor(),
                hasValidGettersAndSetters(),
                hasValidBeanHashCode(),
                hasValidBeanEquals(),
                hasValidBeanToString()
        ));
    }
}
