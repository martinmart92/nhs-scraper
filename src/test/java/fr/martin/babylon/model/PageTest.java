package fr.martin.babylon.model;

import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertThat;
/**
 * testing {@link Page} class
 * Created by martin on 06/08/16.
 */
public class PageTest {


    @Test
    public void testBean() {
        assertThat(Page.class, allOf(
                hasValidBeanConstructor(),
                hasValidGettersAndSetters(),
                hasValidBeanHashCode(),
                hasValidBeanEquals(),
                hasValidBeanToString()
        ));
    }
}
