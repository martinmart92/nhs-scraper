package fr.martin.babylon.logics.impl;

import fr.martin.babylon.ScrapingException;
import fr.martin.babylon.download.DownloaderIfc;
import fr.martin.babylon.logics.ScrapingLogicIfc;
import fr.martin.babylon.model.ConceptPage;
import fr.martin.babylon.model.Page;
import org.easymock.EasyMockSupport;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * testing {@link ConceptPageScrapingLogicImpl} class
 * Created by martin on 06/08/16.
 */
public class ConceptPageScrapingLogicImplTest extends EasyMockSupport {

    @Test
    public void testScrap() throws ScrapingException, IOException {

        Document mockDoc = mock(Document.class);
        expect(mockDoc.title()).andReturn("title value");
        expect(mockDoc.html()).andReturn("html value");
        ConceptPageScrapingLogicImpl tested = new ConceptPageScrapingLogicImpl();
        DownloaderIfc downloader = mock(DownloaderIfc.class);
        Elements elements = new Elements();
        Element mockEl = mock(Element.class);
        elements.add(mockEl);
        expect(mockEl.text()).andReturn("title value");

        expect(mockDoc.select(".healthaz-header h1")).andReturn(elements);

        Elements descriptionElements = new Elements();
        Element mockDescrEl = mock(Element.class);
        descriptionElements.add(mockDescrEl);
        expect(mockDescrEl.attr("content")).andReturn("description value");
        expect(mockDoc.select("head meta[name='DC.description']")).andReturn(descriptionElements);

        Elements keywordsElements = new Elements();
        Element mockKeywordsEl = mock(Element.class);
        keywordsElements.add(mockKeywordsEl);
        expect(mockKeywordsEl.attr("content")).andReturn("keywords value");
        expect(mockDoc.select("head meta[name='keywords']")).andReturn(keywordsElements);


        Elements elementsSectionsLinks = new Elements();
        Element mockSectionEl = mock(Element.class);
        elementsSectionsLinks.add(mockSectionEl);
        expect(mockSectionEl.attr("href")).andReturn("http://section-link-value");

        expect(mockDoc.select("#ctl00_PlaceHolderMain_articles a")).andReturn(elementsSectionsLinks);


        expect(downloader.getUpdatedUrl()).andReturn("updated url");
        tested.init("urlvalue", downloader);

        expect(downloader.get("urlvalue")).andReturn(mockDoc);
        replayAll();
        Page actual = tested.scrap();
        List<ScrapingLogicIfc> nexts = tested.nextLogics();
        verifyAll();

        assertEquals("updated url", actual.getUrl());
        assertEquals("html value", actual.getHtml());
        assertEquals("title value", actual.getTitle());
        assertEquals("description value", actual.getDescription());
        assertEquals("keywords value", actual.getKeywords());
        assertTrue(actual.getUuid()!=null);
        assertEquals(ConceptPage.class, actual.getClass());

        assertEquals(1, nexts.size());
        assertTrue(nexts.get(0) instanceof ConceptSectionPageScrapingLogicImpl);
        ConceptSectionPageScrapingLogicImpl scrapingForSection = (ConceptSectionPageScrapingLogicImpl)nexts.get(0);
        assertEquals("http://section-link-value", scrapingForSection.getUrl());
    }
}
