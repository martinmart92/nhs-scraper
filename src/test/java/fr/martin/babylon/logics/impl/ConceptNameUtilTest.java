package fr.martin.babylon.logics.impl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by martin on 13/08/16.
 */
public class ConceptNameUtilTest {

    @Test
    public void test() {

        ConceptNameUtil util = new ConceptNameUtil();

        assertEquals("snoring", util.processHeader("Symptoms of snoring"));
        assertEquals("ataxia", util.processHeader("Causes of ataxia"));
        assertEquals("benign brain tumour", util.processHeader("Causes of a benign brain tumour"));
        assertEquals("hypotonia", util.processHeader("Diagnosing hypotonia"));
        assertEquals("anorexia", util.processHeader("Treating anorexia"));
        assertEquals("albinism", util.processHeader("Treatment and support for people with albinism"));
        assertEquals("corticobasal degeneration", util.processHeader("Treatment of corticobasal degeneration"));
        assertEquals("rhesus disease", util.processHeader("Complications of rhesus disease"));
        assertEquals("mastectomy", util.processHeader("Complications of a mastectomy"));
        assertEquals("Huntington's disease", util.processHeader("Features of Huntington's disease"));
        assertEquals("androgen insensitivity syndrome", util.processHeader("Living with androgen insensitivity syndrome"));
        assertEquals("severe head injury", util.processHeader("Recovering from a severe head injury"));


        //assertEquals("colostomy", util.processHeader("How a colostomy is performed"));
        //assertEquals("gastroscopy", util.processHeader("Why a gastroscopy is used"));


        //keep same if not heuristic did not find anything interesting
        assertEquals("Congenital heart disease", util.processHeader("Congenital heart disease"));
        assertEquals("Bronchiolitis", util.processHeader("Bronchiolitis"));
        assertEquals("Cancer", util.processHeader("Cancer"));

    }
}
