# NHS Scraper

## Description : 

scraps from http://www.nhs.uk/conditions/pages/hub.aspx, 
navigate on each letter, then on each sub page, parsing metadata and extracting information in title, html, etc.

##compile 

```
mvn clean install
```

##run : 
```
java -jar target/nhs-scraper-1.0-SNAPSHOT.jar
```

##output

A list of json files is generated : {path}/nhs-scraper/tmp/output

See the Constant file to change the hard-coded output {path} (should be changed in the rest service too)